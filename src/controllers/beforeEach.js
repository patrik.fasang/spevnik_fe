import auth from './requests/auth'

const beforeEach = (to, from, next) => {
    // login & register page, let go!
    auth
        .isLoggedIn()
        .then(token => {
            auth.setHttpToken(token) // set to HTTP after refresh
            return next()
        })
        .catch(() => {
            if (to.meta.needsAuth === true) {
                console.log('missing token -> logout')
                auth.logout();
                return next({
                    name: 'login'
                })
            }
            return next();
        });
};

export default beforeEach
