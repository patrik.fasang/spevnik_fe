const userRole = Object.freeze({
    reader:   1,
    writer:  2,
    admin: 3
});

export default userRole;
