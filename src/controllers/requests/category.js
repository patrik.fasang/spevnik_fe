import axios from "axios/index";

const proxyUrl = "/api/categories";

export default class Requester {
    getAllCategories = async () => {
        let result = await axios.get(`${proxyUrl}`);
        return result.data;
    }
}
