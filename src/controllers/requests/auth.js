import userRole from "../enums.js"
import axios from 'axios/index'
import localforage from 'localforage' // localstorage

const auth = {
    saveToken(token) {
        return new Promise((resolve, reject) => {
            localforage.setItem('authtoken', token).then(() => {
                this.setHttpToken(token)
                resolve()
            }).catch(() => {
                reject(new Error('Çannot save token'))
            });
        })
    },

    setHttpToken(token) {
        if (token && !axios.defaults.headers.common['token']) {
            axios.defaults.headers.common['token'] = token
        }
    },

    isLoggedIn() {
        return new Promise((resolve, reject) => {
            localforage.getItem('authtoken').then(token => {
                if (token) {
                    resolve(token)
                } else {
                    reject()
                }
            })
        })
    },

    logout() {
        axios.defaults.headers.common['token'] = '';
        localforage.removeItem('authtoken');
        localforage.removeItem('user');
    },

    loginUser(user) {
        return new Promise((resolve, reject) => {
            axios
                .post('api/auth', user)
                .then(({data}) => {
                    if (data && data.token) {
                        auth.saveToken(data.token).then(() => {
                            resolve()
                        });
                        auth.saveUserData(data.user);
                    } else {
                        reject(new Error('Empty token'))
                    }
                })
                .catch(err => {
                    reject(err)
                })
        })
    },

    hasPermission(permission) {
        return new Promise((resolve, reject) => {
            auth.isLoggedIn().then(async () => {
                let data = await localforage.getItem("user");
                if (data.userRole === permission || data.userRole === userRole.admin) {
                    resolve();
                }
            }).catch(() => {
                reject();
            });
        })
    },

    isCreator(creator_id) {
        return new Promise((resolve, reject) => {
            auth.isLoggedIn().then(async () => {
                let data = await localforage.getItem("user");
                if (data.id === creator_id || data.userRole === userRole.admin) {
                    resolve();
                }
                else {
                    reject()
                }
            }).catch(() => {
                reject();
            });
        })
    },

    saveUserData(userData) {
        localforage.setItem('user', userData)
    }
};

axios.interceptors.response.use(
    response => {
        return response
    },
    error => {
        if (error.response.status === 401) {
            window.location.href = '/'
        }
        return Promise.reject(error)
    }
)

export default auth
