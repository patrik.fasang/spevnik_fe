import axios from "axios/index";

const proxyUrl = "/api/songs";

export default class Requester {
    getAllSongs = async () => {
        let result = await axios.get(`${proxyUrl}`);
        let data = result.data;
        return data;
    };
    getSongDetail = async (id) => {
        let result = await axios.get(`${proxyUrl}/${id}`);
        let data = result.data;
        return data;
    };
    postNewSong = async (form) => {
        let result = await axios.post(`${proxyUrl}`, form);
        let data = result.data;
        return data;
    };
    putUpdateSong = async (form, id) => {
        let result = await axios.patch(`${proxyUrl}/${id}`, form);
        let data = result.data;
        return data;
    };
    deleteSong = async (id) => {
        let result = await axios.delete(`${proxyUrl}/${id}`);
        let data = result.data;
        return data;
    }
}

