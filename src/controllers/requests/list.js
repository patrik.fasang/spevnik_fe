import axios from "axios/index";

const proxyUrl = "/api/lists";

export default class Requester {
    getAllLists = async () => {
        let result = await axios.get(`${proxyUrl}`);
        return result.data;
    };
    postNewList = async (data) => {
        let result = await axios.post(`${proxyUrl}`, data);
        return result.data;
    }
}
