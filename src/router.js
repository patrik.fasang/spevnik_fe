import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: () => import('./layouts/LoginLayout')
        },
        {
            path: '',
            component: () => import('./layouts/DefaultLayout'),
            children: [
                {
                    path: '/',
                    name: 'home',
                    component: Home
                },
                {
                    path: '/songs',
                    component: () => import('./layouts/RouterLayout'),
                    children: [
                        {
                            path: '',
                            name: 'Songs',
                            component: () => import('./views/Home.vue'),
                        },
                        {
                            path: 'add',
                            name: 'EditorAdd',
                            component: () => import('./views/Song/Editor.vue'),
                            meta: {
                                needsAuth: true
                            },
                        },
                        {
                            path: 'edit',
                            name: 'EditorEdit',
                            component: () => import('./views/Song/Editor.vue'),
                            props: true,
                            meta: {
                                needsAuth: true
                            },
                        },
                        {
                            path: ':id',
                            name: 'Detail',
                            component: () => import('./views/Song/Detail.vue'),
                        },
                    ]
                },
            ]
        }
    ]
})
