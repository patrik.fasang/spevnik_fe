import Vue from 'vue'
import App from './App.vue'
import router from './router'

import 'bootstrap/dist/css/bootstrap.css'
import beforeEach from "./controllers/beforeEach";
import './plugins/element.js'
import ReadMore from "vue-read-more";

Vue.use(ReadMore);
Vue.config.productionTip = false;
router.beforeEach(beforeEach);

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
